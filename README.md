# Generators for Laravel.
Includes an extension of Laracademy's interactive make command that covers all the below commands.
Available Commands:
* Make:Pivot
* Make:Package
* Make:Trait
* Make:Resource
* Make:Query

Future Commands:
* Make:Translation
## Installation

## Support on Beerpay
[![Beerpay](https://beerpay.io/Zamerick/generators/badge.svg?style=beer-square)](https://beerpay.io/Zamerick/generators)  
Like this project? Want to say thanks? You can buy me a beer (or coffee) on BeerPay. Thanks and enjoy the generators! :)  

## Roadmap
    * More Documentation
    * Tests
    * Make:translation
    * make:repository
    * make:view