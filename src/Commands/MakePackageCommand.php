<?php
namespace Zamerick\Generators\Commands;

use File;
use Illuminate\Console\Command;
use Zamerick\Generators\Traits\Utility;

class MakePackageCommand extends Command
{
    use Utility;
    protected $signature = 'make:package {name}';
    protected $description = 'Creates a barebones package folder in the vendor directory. Expects a name in the format of VendorName\PackageName';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // do our string manipulation for the various names.
        $names = explode("\\", strtolower($this->argument('name')));
        $vendorName = $names[0];
        $packageName = $names[1];
        $className = ucfirst($packageName) . 'ServiceProvider';

        // Create the test and src directories.
        $this->info('Creating package directory...');
        File::makeDirectory(base_path('vendor/'. $vendorName. '/' . $packageName . '/src'), 777, true, true);
        File::makeDirectory(base_path('vendor/'. $vendorName. '/' . $packageName . '/tests'), 777, true, true);

        // Add our basic files
        $this->createComposerFile($vendorName, $packageName, $classname);
        $this->createServiceProviderFile($vendorName, $packageName, $className);
    }

    /**
     * Grab the composer.json file stub and insert our vendor and package name before
     * writing it to the package directory.
     * @param string $vendorName
     * @param string $packageName
     * @param string $classname
     */
    protected function createComposerFile(string $vendorName, string $packageName, string $className)
    {
        $this->info('Creating basic composer.json file...');
        $template = file_get_contents(base_path('vendor/zamerick/generators/src/Templates/composer.json.stub'));
        $template = Utility::replacePlaceholder($template, '#VENDORNAME#', $vendorName);
        $template = Utility::replacePlaceholder($template, '#PACKAGENAME#', $packageName);
        $template = Utility::replacePlaceholder($template, '#PACKAGENAME#', $className);
        $file = base_path('vendor/'. $vendorName. '/' . $packageName . '/composer.json');
        file_put_contents($file, $template);
    }

    /**
     * Grab the service provider file stub and insert our vendor name, package name,
     * and class name before writing it to the package directory.
     * @param string $vendorName
     * @param string $packageName
     * @param string $classname
     */
    protected function createServiceProviderFile(string $vendorName, string $packageName, string $className)
    {
        $this->info('Creating basic composer.json file...');
        $template = file_get_contents(base_path('vendor/zamerick/generators/src/Templates/provider.stub'));
        $template = Utility::replacePlaceholder($template, '#CLASSNAME#', $className);
        $template = Utility::replacePlaceholder($template, '#VENDORNAME#', $vendorName);
        $template = Utility::replacePlaceholder($template, '#PACKAGENAME#', $packageName);
        $file = base_path('vendor/'. $vendorName. '/' . $packageName .'/' . $className. '.php');
        file_put_contents($file, $template);
    }
}
