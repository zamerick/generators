<?php
namespace Zamerick\Generators\Commands;

use Illuminate\Console\Command;
use File;
use Zamerick\Generators\ConfigItem;
use Zamerick\Generators\Traits\Utility;

class MakePivotCommand extends Command
{
    use Utility;
    protected $signature = 'make:pivot {name}';
    protected $description = 'Creates a cross reference (pivot) table migration. Expects a name in the format of entityone_entitytwo';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // Setup our config object
        $config = new ConfigItem('pivot', $this->argument('name'));

        $this->info('Creating pivot migration...');

        // create the pivot folder in the migrations folder.
        File::makeDirectory(base_path($config->Path), 777, true, true);

        //strip the '.php' off the file name for the migration command
        $name = strtok($config->File, '.');

        // Call the built in make migration command
        $this->call('make:migration', ['name' => $name, '--path' => $config->Path]);

        // Get the contents of the stub file.
        $template = file_get_contents(base_path('vendor/zamerick/generators/src/Templates/pivot.stub'));

        // setup our replacement values
        $tables = array_map('ucfirst', explode("_", strtolower($this->argument('name'))));
        $className = 'Create' . implode(array_map('ucfirst', explode("_", $config->Name))) . 'Table' ;

        // insert our replacement values
        $template = Utility::replacePlaceholder($template, '#CLASSNAME#', $className);
        $template = Utility::replacePlaceholder($template, '#PIVOT#', $config->Name);
        $template = Utility::replacePlaceholder($template, '#TABLEONE#', $tables[0]);
        $template = Utility::replacePlaceholder($template, '#TABLETWO#', $tables[1]);

        // Get the location of the file we created with the migration command
        $file = base_path(implode(glob($config->Path . '/****_**_**_******_' . $config->File)));

        // Store the 'compiled' stub contents in the file.
        file_put_contents($file, $template);
    }
}
