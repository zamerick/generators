<?php
namespace Zamerick\Generators\Commands;

use Illuminate\Console\Command;
use Doctrine\Common\Inflector\Inflector;
use Zamerick\Generators\Traits\Utility;

class MakeQueryCommand extends Command
{
    use Utility;
    private $config = [];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:query {query}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates a query object in App\Queries';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Setup our config object
        $config = new ConfigItem('query', strtolower($this->argument('name')));
        
        // create the query folder in the app folder.
        File::makeDirectory(base_path($config->Path), 777, true, true);

        // Get the contents of the stub file.
        $template = file_get_contents(base_path('vendor/zamerick/generators/src/Templates/query.stub'));

        // Replace the placeholder text in the stub
        $template = Utility::replacePlaceholder($template, '#CLASSNAME#', $config->Name);
        $template = Utility::replacePlaceholder($template, '#NAMESPACE#', $config->Namespace);
        
        // Build the path to our output file.
        $file = base_path($config->Path . $config->File);

        // Write transformed stub to disk.
        file_put_contents($file, $template);
    }
}
