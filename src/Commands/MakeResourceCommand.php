<?php

namespace Zamerick\Generators\Commands;

use File;
use Illuminate\Console\Command;
use Zamerick\Generators\ConfigItem;
use Zamerick\Generators\Traits\Utility;

class MakeResourceCommand extends Command
{
    use Utility;
    /**
     * The name and signature of the console command.
     * @var string
     */
    protected $signature = 'make:resource {resource} 
                            {--R|repository : Use this option to generate repositories instead of models.} 
                            {--views : Use to generate boiler plate views for the resource.}';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Calls other commands to build out migration, controller, model, seeder, and test classes. Also appends templates for factory, seeder call and route to corresponding files.';

    /**
     * Create a new command instance.
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle()
    {
        // Determine if we're building a model or a repository.
        if ($this->option('repository')) {
            $repositoryConfig = new ConfigItem('repository', $this->argument('resource'));
            $this->createRepository($repositoryConfig);
        } else {
            $modelConfig = new ConfigItem('model', $this->argument('resource'));
            $this->createModel($modelConfig);
        }
        // Check for view option
        if ($this->option('views')) {
            $viewConfig = new ConfigItem('view', $this->argument('resource'));
            $this->createViews($viewConfig);
        }
        // Create ConfigItem objects
        $controllerConfig = new ConfigItem('controller', $this->argument('resource'));
        $testConfig = new ConfigItem('test', $this->argument('resource'));
        $migrationConfig = new ConfigItem('migration', $this->argument('resource'));
        $seederConfig = new ConfigItem('seeder', $this->argument('resource'));
        $factoryConfig = new ConfigItem('factory', $this->argument('resource'));
        // do the work
        $this->createController($controllerConfig);
        $this->createMigration($migrationConfig);
        $this->createSeeder($seederConfig);
        $this->createFactory($factoryConfig);
        $this->createTest($testConfig);
        $this->addRouteToApi();
    }
    // Create Model
    protected function createModel(ConfigItem $modelConfig)
    {
        $this->info('Creating Model...');
        $this->call('make:model', ['name' => $modelConfig->Name]);
        // Create Models directory if it doesn't exist
        File::makeDirectory($modelConfig->Path, 777, true, true);
        Utility::moveFile($modelConfig);
    }

    // Create a resource controller.
    protected function createController(ConfigItem $controllerConfig)
    {
        $this->info('Creating Controller...');
        $this->call('make:controller', ['name' => $controllerConfig->Name . $controllerConfig->Type, '--resource' => 'default']);
    }

    // Create Migration with table name. Migration is placed in tables directory. If tables directory doesn't exist, its created.
    protected function createMigration(ConfigItem $migrationConfig)
    {
        $this->info('Creating Model and Controller...');
        File::makeDirectory($migrationConfig->Path . 'tables', 777, true, true);
        $this->call('make:migration', ['name' => $migrationConfig->File, '--create' => strtolower($migrationConfig->Name), '--path' => $migrationConfig->Path . 'tables']);
    }

    // Create the Seeder
    protected function createSeeder(ConfigItem $seederConfig)
    {
        $this->info('Creating Seeder...');
        $this->call('make:seeder', [ 'name' => $seederConfig->File ]);
        $this->addSeederCall($seederConfig);
    }

    // Insert call to table seeder to the DatabaseSeeder.php file
    protected function addSeederCall(ConfigItem $seederConfig)
    {
        $this->info('Adding template to DatabaseSeeder.php...');
        $file = base_path('database/seeds/DatabaseSeeder.php');
        $lines = file($file, FILE_IGNORE_NEW_LINES| FILE_SKIP_EMPTY_LINES);
        $template = '        $this->call(' . $seederConfig->Name . '::class);';
        array_splice($lines, -2, count($lines), array($template, '    }', '}'));
        $contents = implode("\n", $lines);
        file_put_contents($file, $contents, LOCK_EX);
    }

    // Insert Resource route into routes/api.php
    protected function addRouteToApi()
    {
        $this->info('Adding resource route to Routes/api.php...');
        $controllerName = strtolower($this->argument('resource')) .'Controller';
        $file = base_path('routes/api.php');
        $newRoute = 'Route::resource(\''. strtolower($this->argument('resource')) .'\', \''. $controllerName .'\');';
        file_put_contents($file, $newRoute, FILE_APPEND | LOCK_EX);
    }
    // Create test
    protected function createTest(ConfigItem $testConfig)
    {
        $this->info('Creating Test...');
        $this->call('make:test', [ 'name' => $testConfig->Name ]);
    }

    // Create factory
    protected function createFactory(ConfigItem $factoryConfig)
    {
        $this->info('generating factory...');
        $this->call('make:factory', ['name'=>$factoryConfig->Name]);
    }

    // Create Optional Views
    protected function createViews(ConfigItem $viewConfig)
    {
        $this->info('Option not implemented yet');
    }
    // Create Optional Repository
    protected function createRepository(ConfigItem $repositoryConfig)
    {
        $this->info('Option not implemented yet');
    }
}
