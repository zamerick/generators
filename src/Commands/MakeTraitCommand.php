<?php

namespace Zamerick\Generators\Commands;

use Illuminate\Console\Command;
use Zamerick\Generators\ConfigItem;

class MakeTraitCommand extends Command
{
    protected $signature = 'make:trait {name}';
    protected $description = 'Creates an empty trait in App\Traits ';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // Setup our config object
        $config = new ConfigItem('trait', strtolower($this->argument('name')));

        $this->info('Creating trait in App/Traits...');

        // create the trait folder in the app folder.
        File::makeDirectory(base_path($config->Path), 777, true, true);

        // Get the contents of the stub file.
        $template = file_get_contents(base_path('vendor/zamerick/generators/src/Templates/trait.stub'));

        // Replace the placeholder text in the stub
        $template = Utility::replacePlaceholder($template, '#NAME#', $config->Name);

        // Build the path to our output file.
        $file = base_path($config->Path . $config->File);

        // Write transformed stub to disk.
        file_put_contents($file, $template);
    }

}

