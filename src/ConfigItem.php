<?php
namespace Zamerick\Generators;

class ConfigItem
{
    public $Name;
    public $Type;
    public $Path;
    public $File;
    public $Error;
    public $Warning;
    public $Namespace;
    public $DefaultPath;

    public function __construct(string $type, string $name)
    {
        $name = strtolower($name);
        $this->Name = ucfirst($name);
        $this->Type = strtolower($type);
        $this->File = $this->Name . ucfirst($this->Type) . '.php';
        $this->Error = 'could not create ' . $this->Type . ', filename exists.';
        $this->Warning = 'the abstract ' . $this->Type . ' already exists.';
        $this->Namespace = '';

        switch ($type) {
            case 'test':
                $this->Path = './tests/';
                break;
            
            case 'controller':
                $this->Path = './app/Http/Controllers/';
                $this->Namespace = 'App\Http\Controllers';
                break;
            
            case 'model':
                $this->Path = './app/Models/';
                $this->Namespace = 'App\Models';
                $this->DefaultPath = './app/';
                $this->File = $this->Name . '.php';
                break;
            
            case 'migration':
                $this->Path = './database/migrations/';
                $this->File = 'create_' . str_plural(strtolower($name)) . '_table';
                break;
            
            case 'factory':
                $this->Path = './database/factories/';
                break;
            
            case 'seeder':
                $this->Path = './database/seeds/';
                break;
            
            case 'trait':
                $this->Path = './app/Traits/';
                break;
            
            case 'pivot':
                $this->Path = './database/migrations/pivot';
                $this->File = 'create_' . $name . '_pivot_table.php';
                $this->Name = $name . '_pivot';
                break;
            
            case 'translation':
                $this->Path = './resources/Lang';
                $this->File = $name . '.php';
                $this->Name = $name ;
                break;
            
            case 'query':
                $this->Path = './app/Queries';
                $this->File = $name . 'Query.php';
                $this->Name = $name . 'Query';
                $this->Namespace = 'App\Queries';
                break;
            default:
                throw $this->Error;
        }
    }
}
