<?php
namespace Zamerick\Generators;

use Illuminate\Support\ServiceProvider;

class GeneratorServiceProvider extends ServiceProvider
{
    protected $commands = [
        'command.zamerick.make' => 'Zamerick\Generators\Commands\MakeCommand',
        'command.zamerick.make.resource' => 'Zamerick\Generators\Commands\MakeResourceCommand',
        'command.zamerick.make.pivot' => 'Zamerick\Generators\Commands\MakePivotCommand',
        'command.zamerick.make.trait' => 'Zamerick\Generators\Commands\MakeTraitCommand',
        'command.zamerick.make.package' => 'Zamerick\Generators\Commands\MakePackageCommand',
        'command.zamerick.make.query' => 'Zamerick\Generators\Commands\MakeQueryCommand'
    ];

    /**
     * Bootstrap the application services.
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register the application services.
     * @return void
     */
    public function register()
    {
        foreach ($this->commands as $key => $value) {
            $this->app->singleton($key, function ($app) use ($value) {
                return $app[$value];
            });
            $this->commands($key);
        }
    }
}
