<?php
namespace Zamerick\Generators\Traits;

use Zamerick\Generators\ConfigItem;

trait Utility
{
    public static function replacePlaceholder($fileContent, $target, $payload)
    {
        $fileContent = str_replace($target, $payload, $fileContent);
        return $fileContent;
    }

    public static function moveFile(ConfigItem $configItem)
    {
        $source = $configItem->DefaultPath . $configItem->File;
        $destination = $configItem->Path . $configItem->File;
        rename($source, $destination);
    }
}